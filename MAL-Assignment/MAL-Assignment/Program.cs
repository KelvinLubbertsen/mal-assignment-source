﻿using MAL_Assignment.Data;
using MAL_Assignment.Domain;
using MAL_Assignment.Process;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAL_Assignment
{
    class Program
    {
        static void Main(string[] args)
        {
            /*Torus torus = new Torus();

            for (int time = 0; time < Config.Instance.AmountOfSeconds; time++)
                torus.Update();

            for(int i = 1; i <= torus.Skaters.Count; i++)
            {
                Console.WriteLine("Skater {0}: {1}", i, torus.Skaters[i - 1].CumulativeReward);
            }

            MetricsWriter writer = new MetricsWriter();
            writer.Write();*/

            //PeopleCountExperiment pce = new PeopleCountExperiment();
            //pce.Run();

            //AverageRewardOverTime arot = new AverageRewardOverTime();
            //arot.Run();

            ProbabilityPlayedAction ppa = new ProbabilityPlayedAction();
            ppa.Run();
            //AverageRewardOverAllActions aroaa = new AverageRewardOverAllActions();
            //aroaa.Run();

            //ConvergenceOverTime cot = new ConvergenceOverTime();
            //cot.Run();

            //EpsilonConvergenceOverTime ecot = new EpsilonConvergenceOverTime();
            //ecot.Run();
        }
    }
}
