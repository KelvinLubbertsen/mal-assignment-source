﻿using MAL_Assignment.Data;
using MAL_Assignment.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAL_Assignment.Process
{
    public class ConvergenceOverTime
    {
        public void Run()
        {
            for (int x = 1; x <= 7; x += 1)
            {
                Config.Instance.Reward2 = -x;
                Dictionary<int, double> result = new Dictionary<int, double>();
                for (int t = 0; t < Config.Instance.AmountOfPlays; t++)
                    result.Add(t, 0);
                for (int i = 0; i < 30; i++)
                {
                    Console.WriteLine("Experiment {0} (r2={1})", i + 1, -x);
                    Metrics.Clear();
                    SingleRun sr = new SingleRun();
                    sr.Run();
                    Dictionary<int, List<double>> dirs = new Dictionary<int, List<double>>();
                    for (int t = 0; t < Config.Instance.AmountOfPlays; t++)
                        dirs.Add(t, new List<double>());
                    foreach (var skater in Metrics.Instance.Directions.Keys)
                    {
                        for (int t = 0; t < Metrics.Instance.Directions[skater].Count; t++)
                            dirs[t].Add(Metrics.Instance.Directions[skater][t]);
                        //dirs.Add(Metrics.Instance.Directions[skater].Last());
                    }

                    Dictionary<int, Dictionary<double, int>> directionCount = new Dictionary<int, Dictionary<double, int>>();
                    for (int t = 0; t < Config.Instance.AmountOfPlays; t++)
                    {
                        directionCount.Add(t, new Dictionary<double, int>());
                        foreach (var dir in dirs[t])
                        {
                            if (!directionCount[t].ContainsKey(dir))
                                directionCount[t].Add(dir, 1);
                            else
                                directionCount[t][dir]++;
                        }
                    }
                    foreach (var t in directionCount.Keys)
                    {
                        var count = directionCount[t].OrderByDescending(kv => kv.Value).First().Value;
                        result[t] += count;
                    }
                }

                for (int t = 0; t < Config.Instance.AmountOfPlays; t++)
                {
                    result[t] = result[t] / 30;
                }

                ConvergenceOverTimeWriter cotw = new ConvergenceOverTimeWriter();
                Skater tmp = new Skater();
                cotw.Write("../../Results/ConvergenceOverTime/ConvergenceOverTime-" + tmp.Strategy + "-x="+x+".dat", result);
            }
        }
    }
}
