﻿using MAL_Assignment.Data;
using MAL_Assignment.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAL_Assignment.Process
{
    public class PeopleCountExperiment
    {
        public void Run()
        {
            
            for (double speed = 3; speed <= 9; speed += 1)
            {
                List<Tuple<int, double>> avgs = new List<Tuple<int, double>>();
                for (int n = 1; n <= 30; n++)
                {
                    Console.WriteLine("Experiment for speed={1} n={0}", n, speed);
                    List<Metrics> metrics = new List<Metrics>();

                    Config.Instance.AmountOfPeople = n;
                    for (int i = 0; i < 30; i++)
                    {
                        Metrics.Clear();
                        SingleRun sr = new SingleRun();
                        sr.Run();
                        metrics.Add(Metrics.Instance);
                    }

                    double avg = 0;
                    foreach (var metric in metrics)
                    {
                        double sum = 0;
                        for (int i = 1; i <= n; i++)
                            sum += metric.Collisions[i - 1];

                        avg = sum / (Config.Instance.AmountOfPlays * Config.Instance.AmountOfPeople);
                    }
                    avgs.Add(new Tuple<int, double>(n, avg));
                }

                PeopleCountExperimentWriter pcew = new PeopleCountExperimentWriter();

                pcew.Write("../../Results/PeopleCountExperiment/result-"+ speed +".dat", avgs);
            }
        }
    }
}
