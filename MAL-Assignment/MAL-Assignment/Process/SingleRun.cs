﻿using MAL_Assignment.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAL_Assignment.Process
{
    public class SingleRun
    {
        public void Run()
        {
            Torus torus = new Torus();

            for (int time = 0; time < Config.Instance.AmountOfPlays; time++)
                torus.Update();
        }
    }
}
