﻿using MAL_Assignment.Data;
using MAL_Assignment.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAL_Assignment.Process
{
    public class ProbabilityPlayedAction
    {
        public void Run()
        {
            List<Dictionary<int, Dictionary<double, int>>> results = new List<Dictionary<int, Dictionary<double, int>>>();

            for(int i = 0; i < 1; i++)
            {
                Console.WriteLine("Experiment {0}", i + 1);
                Metrics.Clear();
                SingleRun sr = new SingleRun();
                sr.Run();
                results.Add(Metrics.Instance.AmountOfTimesPlayedAction);
            }
            List<double> actions = new List<double>();
            var n = 360 / Config.Instance.AmountOfActions;
            for (double i = 0; i < 360; i += n)
                actions.Add(i);

            Dictionary<int, Dictionary<double, double>> rtrn = new Dictionary<int, Dictionary<double, double>>();

            for (int i = 0; i < Config.Instance.AmountOfPlays; i++)
            {
                //Torus tmp = new Torus();
                rtrn.Add(i, new Dictionary<double, double>());
                foreach (var action in actions)
                    rtrn[i].Add(action, 0);
            }

            foreach(var res in results)
            {
                for (int i = 0; i < Config.Instance.AmountOfPlays; i++)
                {
                    foreach (var action in res[i].Keys)
                        rtrn[i][action] += res[i][action];
                }
            }


            for (int i = 0; i < Config.Instance.AmountOfPlays; i++)
            {
                foreach (var action in actions)
                    rtrn[i][action] = rtrn[i][action] / 1d / Config.Instance.AmountOfPeople;
            }

            ProbabilityPlayedActionWriter ppaw = new ProbabilityPlayedActionWriter();
            Skater tmp = new Skater();
            ppaw.Write("../../Results/ProbabilityPlayedAction-"+tmp.Strategy+".dat", rtrn);
        }
    }
}
