﻿using MAL_Assignment.Data;
using MAL_Assignment.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAL_Assignment.Process
{
    public class AverageRewardOverTime
    {
        public void Run()
        {
            List<Dictionary<double, Dictionary<int, double>>> result = new List<Dictionary<double, Dictionary<int, double>>>();
            for(int i = 0; i < 30; i++)
            {
                Console.WriteLine("Experiment {0}", i + 1);
                Metrics.Clear();
                SingleRun sr = new SingleRun();
                sr.Run();

                result.Add(Metrics.Instance.AverageRewardOverTime);
            }

            Dictionary<double,Dictionary<int,double>> retrn = new Dictionary<double, Dictionary<int, double>>();

            //list of sums
            for (double n = 0; n < 360; n+= (360 / Config.Instance.AmountOfActions))
            {
                retrn.Add(n, new Dictionary<int, double>());
                for (int t = 0; t < Config.Instance.AmountOfPlays; t++)
                    retrn[n].Add(t, 0);
            }

            foreach(var run in result)
            {
                foreach (var action in run.Keys)
                {
                    foreach (var time in run[action].Keys)
                        retrn[action][time] += run[action][time];
                }
            }

            //list of means
            foreach (var action in retrn.Keys)
            {
                for (int t = 0; t < Config.Instance.AmountOfPlays; t++)
                    retrn[action][t] = retrn[action][t] / 30;
            }

            AverageRewardOverTimeWriter arotw = new AverageRewardOverTimeWriter();
            Skater tmp = new Skater();
            arotw.Write("../../Results/AverageRewardOverTimeWriter-"+tmp.Strategy+".dat", retrn);
        }
    }
}
