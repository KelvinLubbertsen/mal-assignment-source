﻿using MAL_Assignment.Data;
using MAL_Assignment.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAL_Assignment.Process
{
    public class AverageRewardOverAllActions
    {
        public void Run()
        {
            for(int n = 5; n <= 25; n += 5)
            {
                Config.Instance.AmountOfPeople = n;

                Dictionary<int, double> sums = new Dictionary<int, double>();
                for (int t = 0; t < Config.Instance.AmountOfPlays; t++)
                    sums.Add(t, 0);

                for (int i = 0; i < 30; i++)
                {
                    Console.WriteLine("Experiment {0} (N={1})", i + 1, n);
                    Metrics.Clear();
                    SingleRun sr = new SingleRun();
                    sr.Run();
                    for(int t = 0; t < Config.Instance.AmountOfPlays; t++)
                    {
                        sums[t] += Metrics.Instance.RewardsPerPlay[t].Average();
                    }
                }

                Dictionary<int, double> avgs = new Dictionary<int, double>();
                for (int t = 0; t < Config.Instance.AmountOfPlays; t++)
                    avgs.Add(t, sums[t] / 30);

                AverageRewardOverAllActionsWriter aroaaw = new AverageRewardOverAllActionsWriter();
                Skater tmp = new Skater();
                aroaaw.Write("../../Results/AverageRewardOverAllActions/AverageRewardOverAllActions-"+tmp.Strategy+"-" + n+".dat", avgs);
            }
        }
    }
}
