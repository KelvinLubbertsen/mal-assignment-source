﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MAL_Assignment.Domain.ActionPicking;

namespace MAL_Assignment.Domain
{
    public class Skater
    {
        public int ID { get; set; }
        public double X { get; set; }
        public double Y { get; set; }
        public double CumulativeReward { get; set; }
        public double Action { get; set; }
        public double Reward { get; set; }
        public IActionPickingStrategy Strategy { get; set; }

        public Skater()
        {
            Strategy = new EpsilonGreedyLearning();
            X = Config.Instance.Random.NextDouble() * Config.Instance.TorusWidth;
            Y = Config.Instance.Random.NextDouble() * Config.Instance.TorusHeight;

            
        }
        public Skater(double x, double y)
        {
            X = x;
            Y = y;
        }

        public void Update(Torus torus)
        {
            var action = Strategy.PickAction(this, torus);
            Action = action;
            var radian = ToRadian(action);

            var xDiff = Math.Cos(radian) * Config.Instance.SkateSpeed;
            var yDiff = Math.Sin(radian) * Config.Instance.SkateSpeed;

            var tmpSkater = new Skater(X + xDiff, Y + yDiff);

            bool collision = false;

            foreach (var skater in torus.Skaters)
            {
                if (skater == this)
                    continue;
                else
                {
                    if (DistanceWithOtherSkater(tmpSkater, skater) < Config.Instance.CollisionRadius)
                    {
                        collision = true;
                        break;
                    }
                }
            }
            double reward;
            if (collision)
            {
                reward = Config.Instance.Reward2;
                Metrics.Instance.Collisions[ID]++;
            }
            else
            {
                reward = Config.Instance.Reward1;
                X += xDiff;
                Y += yDiff;
                if (X > Config.Instance.TorusWidth)
                    X -= Config.Instance.TorusWidth;
                else if (X < 0)
                    X += Config.Instance.TorusWidth;

                if (Y > Config.Instance.TorusHeight)
                    Y -= Config.Instance.TorusHeight;
                else if (Y < 0)
                    Y += Config.Instance.TorusHeight;
            }

            CumulativeReward += reward;
            Reward = reward;
            Strategy.InformAction(action, reward);
            Metrics.Instance.Rewards[action].Add(reward);
            Metrics.Instance.AmountOfTimesPlayedAction[torus.Time][action]++;

            Metrics.Instance.Directions[this].Add(action);
            Metrics.Instance.Positions[this].Add(new double[] { X, Y });
        }

        private double ToRadian(double degree)
        {
            return degree * (Math.PI / 180);
        }

        public double DistanceWithOtherSkater(Skater first, Skater second)
        {
            return Math.Sqrt(
                Math.Pow(
                    Math.Min(
                        Math.Abs(
                            first.X - second.X),
                        Config.Instance.TorusWidth - Math.Abs(
                            first.X - second.X)
                    ),
                2) +
                Math.Pow(
                    Math.Min(
                        Math.Abs(
                            first.Y - second.Y),
                        Config.Instance.TorusHeight - Math.Abs(
                            first.Y - second.Y)
                        )
                    , 2));
        }
    }
}
