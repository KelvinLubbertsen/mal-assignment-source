﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAL_Assignment.Domain.ActionPicking
{
    public class RandomStrategy : IActionPickingStrategy
    {
        public void InformAction(double action, double reward)
        {
            // do nothing
        }

        public double PickAction(Skater skater, Torus torus)
        {
            return torus.A[Config.Instance.Random.Next(torus.A.Length)];
        }

        public override string ToString()
        {
            return "random";
        }
    }
}
