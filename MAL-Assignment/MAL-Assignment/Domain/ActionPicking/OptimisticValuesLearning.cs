﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAL_Assignment.Domain.ActionPicking
{
    public class OptimisticValuesLearning : IActionPickingStrategy
    {
        private bool _initialized = false;
        private Dictionary<double, double[]> _scores;
        private void Init(Torus torus)
        {
            _scores = new Dictionary<double, double[]>();

            var n = 1;
            //var sum = (Math.Abs(Config.Instance.Reward1) + Math.Abs(Config.Instance.Reward2)) * 2;
            var sum = Config.Instance.HighReward;
            foreach (var action in torus.A)
            {
                _scores.Add(action, new double[] { sum, n });
            }
            _initialized = true;
        }
        public void InformAction(double action, double reward)
        {
            _scores[action][0] += reward;
            _scores[action][1]++;
        }

        public double PickAction(Skater skater, Torus torus)
        {
            if (!_initialized)
                Init(torus);

            var bestAvg = double.MinValue;
            var bestAction = new List<double>();

            foreach(var action in _scores.Keys)
            {
                double avg = _scores[action][0] / _scores[action][1];

                if (bestAvg <= avg)
                {
                    if (bestAvg < avg)
                        bestAction.Clear();

                    bestAvg = avg;
                    bestAction.Add(action);
                }
            }

            return bestAction[Config.Instance.Random.Next(bestAction.Count)];
        }

        public override string ToString()
        {
            return "optimistic";
        }
    }
}
