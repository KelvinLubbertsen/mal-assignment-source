﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAL_Assignment.Domain.ActionPicking
{
    public interface IActionPickingStrategy
    {
        double PickAction(Skater skater, Torus torus);
        void InformAction(double action, double reward);
    }
}
