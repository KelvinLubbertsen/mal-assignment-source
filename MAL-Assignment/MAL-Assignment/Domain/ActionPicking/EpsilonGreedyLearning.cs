﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAL_Assignment.Domain.ActionPicking
{
    public class EpsilonGreedyLearning : IActionPickingStrategy
    {
        private bool _initialized = false;
        private Dictionary<double, List<double>> _history;
        public EpsilonGreedyLearning()
        {
        }
        private void Init(Torus torus)
        {
            _history = new Dictionary<double, List<double>>();
            foreach(double action in torus.A)
                _history.Add(action, new List<double>());
            _initialized = true;
        }
        public void InformAction(double action, double reward)
        {
            _history[action].Add(reward);
        }

        public double PickAction(Skater skater, Torus torus)
        {
            if (!_initialized)
                Init(torus);

            var sample = Config.Instance.Random.NextDouble();
            var epsilon = GetEpsilon(torus.Time);
            if(1-sample > epsilon)
            {
                //exploit
                double bestAverage = double.MinValue;
                List<double> bestAction = new List<double>();
                foreach(var action in _history.Keys)
                {
                    double avg = 0;
                    if(_history[action].Count > 0)
                        avg = _history[action].Average();
                    
                    if (bestAverage <= avg)
                    {
                        if (bestAverage < avg)
                            bestAction.Clear();

                        bestAverage = avg;
                        bestAction.Add(action);
                    }
                }
                return bestAction[Config.Instance.Random.Next(bestAction.Count)];
            }
            else
            {
                //explore
                return torus.A[Config.Instance.Random.Next(torus.A.Length)];
            }
        }

        private double GetEpsilon(int time)
        {
            if (Config.Instance.AdaptiveEpsilon)
                return Config.Instance.Epsilon * Math.Pow(0.99, (time / 2));
            return Config.Instance.Epsilon;
        }

        public override string ToString()
        {
            var epsilon = "" + Config.Instance.Epsilon;
            epsilon = epsilon.Replace(',', '.');
            string adaptive = "";
            if (Config.Instance.AdaptiveEpsilon)
                adaptive = "-adaptive";
            return "epsilon-e" + epsilon + adaptive;
        }
    }
}
