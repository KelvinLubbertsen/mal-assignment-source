﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAL_Assignment.Domain
{
    public class Torus
    {
        public List<Skater> Skaters { get; set; }
        public double[] A { get; set; }
        public int Time = 0;
        public Torus()
        {
            List<double> a = new List<double>();
            var n = 360 / Config.Instance.AmountOfActions;
            for (double i = 0; i < 360; i += n)
                a.Add(i);
            A = a.ToArray();

            Skaters = new List<Skater>();
            int skaters = 0;
            while(skaters < Config.Instance.AmountOfPeople)
            {
                var skater = new Skater() { ID = skaters };
                bool canPlace = true;
                foreach(var s in Skaters)
                {
                    if(s.DistanceWithOtherSkater(s, skater) <= Config.Instance.CollisionRadius)
                    {
                        canPlace = false;
                        break;
                    }
                }
                if (!canPlace)
                    continue;
                
                Skaters.Add(skater);
                Metrics.Instance.Directions.Add(skater, new List<double>());
                Metrics.Instance.Positions.Add(skater, new List<double[]>());
                skaters++;
            }
            //for (int i = 0; i < Config.Instance.AmountOfPeople; i++)
            //    Skaters.Add(new Skater() { ID = i });

            foreach (var action in A)
            {
                Metrics.Instance.Rewards.Add(action, new List<double>());
                Metrics.Instance.AverageRewardOverTime.Add(action, new Dictionary<int, double>());
            }

            for (int t = 0; t < Config.Instance.AmountOfPlays; t++)
            {
                Metrics.Instance.AmountOfTimesPlayedAction.Add(t, new Dictionary<double, int>());

                foreach (var action in A)
                    Metrics.Instance.AmountOfTimesPlayedAction[t].Add(action, 0);
            }
        }

        public void Update()
        {
            Metrics.Instance.RewardsPerPlay.Add(Time, new List<double>());
            foreach (var skater in Skaters)
            {
                skater.Update(this);
                Metrics.Instance.RewardsPerPlay[Time].Add(skater.Reward);
                
            }
            foreach(var action in Metrics.Instance.Rewards.Keys)
            {
                //var tuple = new Tuple<int, double>(Metrics.Instance.Rewards.Count + 1, Metrics.Instance.Rewards[action].Average());

                double avg;
                if (Metrics.Instance.Rewards[action].Count == 0)
                    avg = 0;
                else
                    avg = Metrics.Instance.Rewards[action].Average();

                Metrics.Instance.AverageRewardOverTime[action].Add(Time, avg);
                
                //Metrics.Instance.AverageRewardOverTime.Add(tuple);
            }

            Time++;
        }
    }
}
