﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAL_Assignment.Domain
{
    public class Metrics
    {
        public Dictionary<Skater, List<double[]>> Positions { get; set; }
        public Dictionary<Skater, List<double>> Directions { get; set; }
        public int[] Collisions { get; set; }
        public Dictionary<double, List<double>> Rewards { get; set; }
        public Dictionary<double,Dictionary<int, double>> AverageRewardOverTime { get; set; }
        public Dictionary<int, Dictionary<double, int>> AmountOfTimesPlayedAction { get; set; }
        public Dictionary<int, List<double>> RewardsPerPlay { get; set; }
        private Metrics()
        {
            Positions = new Dictionary<Skater, List<double[]>>();
            Directions = new Dictionary<Skater, List<double>>();
            Collisions = new int[Config.Instance.AmountOfPeople];
            Rewards = new Dictionary<double, List<double>>();
            AverageRewardOverTime = new Dictionary<double, Dictionary<int, double>>();
            AmountOfTimesPlayedAction = new Dictionary<int, Dictionary<double, int>>();
            RewardsPerPlay = new Dictionary<int, List<double>>();
        }

        #region Singleton
        private static Metrics _instance;
        public static Metrics Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new Metrics();
                return _instance;
            }
        }
        #endregion
        
        public static void Clear()
        {
            _instance = new Metrics();
        }
    }
}
