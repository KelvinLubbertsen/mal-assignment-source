﻿using MAL_Assignment.Domain;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAL_Assignment.Data
{
    public class MetricsWriter
    {
        public const string DIRECTIONS = "directions.csv";
        public const string POSITIONS = "positions.csv";
        public void Write()
        {
            WriteDirections(DIRECTIONS);
            WritePositions(POSITIONS);
        }
        public void WritePositions(string filename)
        {
            List<string> lines = new List<string>();
            lines.Add(MakeHeader());

            for (int y = 0; y < Config.Instance.AmountOfPlays; y++)
            {
                var line = (y+ 1) + ";";

                foreach (var kv in Metrics.Instance.Positions)
                    line += kv.Value[y][0] + "." + kv.Value[y][1] + ";";
                lines.Add(line);
            }

            File.WriteAllLines(filename, lines.ToArray());
        }

        public void WriteDirections(string filename)
        {
            List<string> lines = new List<string>();
            lines.Add(MakeHeader());

            for(int y = 0; y < Config.Instance.AmountOfPlays; y++)
            {
                var line = (y + 1) + ";";

                foreach (var kv in Metrics.Instance.Directions)
                    line += kv.Value[y] + ";";
                lines.Add(line);
            }

            File.WriteAllLines(filename, lines.ToArray());
        }

        private string MakeHeader()
        {
            string header = "Time;";
            for (int i = 1; i <= Metrics.Instance.Directions.Keys.Count; i++)
                header += i + ";";
            return header;
        }
    }
}
