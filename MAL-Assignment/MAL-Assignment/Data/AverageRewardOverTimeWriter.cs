﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAL_Assignment.Data
{
    public class AverageRewardOverTimeWriter
    {
        public void Write(string filename, Dictionary<double, Dictionary<int, double>> res)
        {
            List<string> lines = new List<string>();
            lines.Add(MakeHeader(res.Keys.Count));

            foreach(int time in res[res.Keys.First()].Keys)
            {
                string line = (time + 1) + " ";

                foreach(var action in res.Keys)
                {
                    line += res[action][time] + " ";
                }

                line = line.Replace(',', '.');

                lines.Add(line);
            }

            File.WriteAllLines(filename, lines.ToArray());
        }

        private string MakeHeader(int actionCount)
        {
            string line = "N ";

            for (int i = 1; i <= actionCount; i++)
                line += string.Format("$P_{0}$ ", i);
            return line;
        }
    }
}
