﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAL_Assignment.Data
{
    public class PeopleCountExperimentWriter
    {
        public void Write(string filename, List<Tuple<int, double>> results)
        {
            List<string> lines = new List<string>();
            lines.Add(MakeHeader());

            foreach(var tuple in results)
            {
                string line = string.Format("{0} {1} {2}", tuple.Item1, tuple.Item2, (1d/3d));
                line = line.Replace(',', '.');
                lines.Add(line);
            }

            File.WriteAllLines(filename, lines.ToArray());
        }
        private string MakeHeader()
        {
            return "N $P_1$ $P_2$";
        }
    }
}
