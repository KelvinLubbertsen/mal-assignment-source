﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAL_Assignment.Data
{
    public class ProbabilityPlayedActionWriter
    {
        public void Write(string filename, Dictionary<int, Dictionary<double, double>> results)
        {
            List<string> lines = new List<string>();
            lines.Add(MakeHeader(results[results.Keys.First()].Keys.Count));

            foreach(var time in results.Keys)
            {
                string line = (time + 1) + " ";
                foreach(var action in results[time].Keys)
                {
                    line += results[time][action] + " ";
                }
                line = line.Replace(',', '.');
                lines.Add(line);
            }

            File.WriteAllLines(filename, lines.ToArray());
        }

        private string MakeHeader(int actionCount)
        {
            string line = "N ";

            for (int i = 1; i <= actionCount; i++)
                line += string.Format("$P_{0}$ ", i);

            return line;
        }
    }
}
