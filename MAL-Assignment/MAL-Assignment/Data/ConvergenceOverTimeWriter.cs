﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAL_Assignment.Data
{
    public class ConvergenceOverTimeWriter
    {
        public void Write(string filename, Dictionary<int, double> result)
        {
            List<string> lines = new List<string>();
            lines.Add(MakeHeader());

            foreach(var t in result.Keys)
            {
                string line = string.Format("{0} {1}", (t + 1), result[t]);
                line = line.Replace(',', '.');
                lines.Add(line);
            }
            File.WriteAllLines(filename, lines.ToArray());
        }
        private string MakeHeader()
        {
            return "N $P_1$";
        }
    }
}
