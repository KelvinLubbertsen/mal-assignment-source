﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAL_Assignment
{
    public class Config
    {
        public double TorusWidth = 10; //width
        public double TorusHeight = 10; //height
        public int AmountOfPeople = 18; //N
        public double AmountOfActions = 6; //k
        public double SkateSpeed = 1; //delta m/s
        public double CollisionRadius = 1; //r
        public double Reward1 = 1; //R1
        public double Reward2 = -2; //R2
        public double HighReward = 10;
        public Random Random = new Random();

        public int AmountOfPlays = 2000;

        public bool AdaptiveEpsilon = true;
        public double Epsilon = 0.15;

        #region Singleton

        private static Config _instance;

        private Config() { }

        public static Config Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new Config();
                return _instance;
            }
        }
        #endregion
    }
}
