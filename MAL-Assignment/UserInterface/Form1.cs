﻿using MAL_Assignment;
using MAL_Assignment.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UserInterface
{
    public partial class Form1 : Form
    {
        Torus _torus;
        public Form1()
        {
            Config.Instance.AmountOfPlays = 100000;
            InitializeComponent();
            _torus = new Torus();
            this.DoubleBuffered = true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            _torus.Update();
            Invalidate();
            string convergence = "";

            var directions = Metrics.Instance.Directions.Last().Value;
            List<double> dirs = new List<double>();
            foreach(var skater in Metrics.Instance.Directions.Keys)
            {
                dirs.Add(Metrics.Instance.Directions[skater].Last());
            }
            Dictionary<double, int> directionCount = new Dictionary<double, int>();
            foreach(var dir in dirs)
            {
                if (!directionCount.ContainsKey(dir))
                    directionCount.Add(dir, 1);
                else
                    directionCount[dir]++;
            }

            var count = directionCount.OrderByDescending(kv => kv.Value).First().Value;
            convergence = string.Format(" convergence: ({0} / {1})", count, Config.Instance.AmountOfPeople);

            Text = string.Format("Time: {0}", _torus.Time) + convergence;
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            e.Graphics.Clear(Color.White);

            DrawTorus(e.Graphics);

            foreach (var skater in _torus.Skaters)
                DrawSkater(e.Graphics, skater);

            base.OnPaint(e);
        }

        private void DrawSkater(Graphics graphics, Skater skater)
        {
            var xScale = Width / Config.Instance.TorusWidth;
            var yScale = Height / Config.Instance.TorusHeight;

            graphics.FillEllipse(Brushes.Black,
                new Rectangle((int)(skater.X * xScale - 2), (int)(skater.Y * yScale - 2), 4, 4));

            var radius = Config.Instance.CollisionRadius;

            graphics.DrawEllipse(Pens.Red, new Rectangle((int)(skater.X * xScale - (radius * xScale)), (int)(skater.Y * yScale - (radius * yScale)), (int)((radius * 2) * xScale), (int)((radius * 2) * yScale)));

            var xDiff = Math.Cos(skater.Action * (Math.PI / 180)) * Config.Instance.SkateSpeed;
            var yDiff = Math.Sin(skater.Action * (Math.PI / 180)) * Config.Instance.SkateSpeed;

            graphics.DrawLine(Pens.Green, new Point((int)(skater.X * xScale), (int)(skater.Y * yScale)), new Point((int)((skater.X + xDiff) * xScale), (int)((skater.Y + yDiff) * yScale)));
        }

        private void DrawTorus(Graphics graphics)
        {
            var xScale = Width / Config.Instance.TorusWidth;
            var yScale = Height / Config.Instance.TorusHeight;

            for(int x = 0; x < Config.Instance.TorusWidth; x++)
            {
                graphics.DrawLine(Pens.Blue, new Point((int)(x * xScale), 0), new Point((int)(x * xScale), Height));
            }
            for (int y = 0; y < Config.Instance.TorusHeight; y++)
            {
                graphics.DrawLine(Pens.Blue, new Point(0, (int)(y * yScale)), new Point(Width, (int)(y * yScale)));
            }
        }
    }
}
